<?php
// $CVS$

include_once('profiles/emspace/ems_basic/ems_basic.profile');

function emspace_profile_modules() {
  return _ems_basic_profile_modules();
}

function emspace_profile_details() {
  return _ems_basic_profile_details();
}

function emspace_profile_final() {
  return _ems_basic_profile_final();
}
