<?php
// $CVS$

define ('EMS_ROLE_ADMINISTRATOR', 'Administrator');
define ('EMS_ROLE_EDITOR', 'Editor');
define ('EMS_ROLE_MAINTAINER', 'Maintainer');
define ('EMS_ROLE_OWNER', 'Owner');
define ('EMS_ROLE_CONTRIBUTOR', 'Contributor');

// We consider this our primary crud function include.
include_once("sites/all/modules/install_profile_api/crud.inc");

// Here are new functions that haven't we haven't offered as patches yet.
//include_once("profiles/emspace/emspace.crud.inc");

/*
 * Implementation of hook_profile_modules()
 */
function _ems_basic_profile_modules() {
  return array(
    'user', 'menu', 'node', 'path', 'watchdog', 'block', 'help', 'color','system',
    'taxonomy', 'filter',
    'content', 'content_copy', 'optionwidgets', 'fieldgroup',
    'nodereference', 'userreference', 'date', 'date_copy', 'date_api', 'number',
    'imagecache', 'imagefield',
    'views', 'views_ui', 'views_rss',
    'workflow', 'actions',
    'panels', 'bueditor',
    'devel', 'pathauto',
    'simplemenu',
    'email', 'link', 'text',
    'upload', 'emspace_profile',
  );
}

function _ems_basic_profile_details() {
  return array(
    'name' => 'Em Space Basic',
    'description' => 'Drupal default install for emspace.com.au.'
  );
}

function _ems_basic_profile_final() {

  /* Theme Settings */
  install_enable_theme('emspace_2007');
  install_enable_theme('hunchbaque');
  variable_set('admin_theme', 'emspace_2007');
  variable_set('theme_default', 'hunchbaque');

  // Apply some blocks to all themes
  $themes = system_theme_data();  
  foreach ($themes AS $info) {
    // Kill the user nav.
    install_set_block($info->name, -1, 'user', 1);
    // Assigning to 'left' and 'sidebar_left' is rough, but harmless.
    install_set_block($info->name, 'left', 'devel', 2);
    install_set_block($info->name, 'sidebar_left', 'devel', 2);
    install_set_block($info->name, 'left', 'emspace_profile', 0);
    install_set_block($info->name, 'sidebar_left', 'emspace_profile', 0);
    install_set_block($info->name, 'right', 'emspace_profile', 1);
    install_set_block($info->name, 'sidebar_right', 'emspace_profile', 1);
  }
  // Semantically good, bad or indifferent?
  install_set_block('hunchbaque', 'main_supplements', 'devel', 2);
  install_set_block('hunchbaque', 'main_supplements', 'emspace_profile', 0);
  install_set_block('hunchbaque', 'secondary_supplements', 'emspace_profile', 1);
  
  // Create content types, then users, so that data is available
  // to the following processes.
  _ems_basic_profile_final_cck();
  _ems_basic_profile_final_user();

  _ems_basic_profile_final_settings();
  _ems_basic_profile_final_menu();
  _ems_basic_profile_final_views();
  _ems_basic_profile_final_workflow();

  // Login as user 1
  user_authenticate('admin', 'changeme');
  drupal_set_message('Logged in as user 1 (admin).');

  return 'Quick links: '.
    l('Content types', 'admin/content/types') .', '.
    l('Users', 'admin/users') .', '.
    l('Change your password', 'user/1/edit');

}

/*
 * Content Types
 */
function _ems_basic_profile_final_cck() {
  // Contact form
  install_create_content_type('contact', 'Contact Form', array('title_label' => 'Name', 'description' => 'Contact form for the website.'));
  install_create_field('contact', 'field_contact_email', 'text-text', 'Email', array('weight' => 1));
  install_create_field('contact', 'field_contact_subject', 'text-options_select', 'Subject', array('allowed_values' => "general|General Enquiry\nfeedback|Website Feedback", 'default_value' => 'general', 'required' => TRUE));
  install_create_field('contact', 'field_contact_message', 'text-text', 'Message', array('rows' => 8));
}

/*
 * Settings
 */
function _ems_basic_profile_final_settings() {
  /* General Settings */

  variable_set('simplemenu_menu', '6');
  variable_set('simplemenu_devel', TRUE);
  variable_set('clean_url', '1');

  //Contact
  variable_set('comment_contact', '0');
  variable_set('upload_contact', '1');
  //Book
  variable_set('comment_book', '0');
  variable_set('upload_book', '1');

  //Other
  variable_set('error_level', '1');

  variable_set('file_directory_path', 'sites/default/files');

  variable_set('upload_max_resolution', '0');
  variable_set('upload_list_default', '1');
  variable_set('upload_extensions_default', 'jpg jpeg gif png txt html doc xls pdf ppt pps odt ods odp mp3 swf flv psd ');
  variable_set('upload_uploadsize_default', '1');
  variable_set('upload_usersize_default', '1');

  variable_set('date_format_short', 'd/m/Y - H:i');
  variable_set('date_format_medium', 'D, d/m/Y - H:i');
  variable_set('date_format_long', 'l, F j, Y - H:i');
  variable_set('date_default_timezone', '36000');

  variable_set('comment_anonymous', '0');

  variable_set('site_name', 'New Drupal Website');

  variable_set('site_mail', 'emspace.com.au@gmail.com');

  variable_set('user_register', '0');
  variable_set('user_email_verification', 1);
}

/*
 * Menu
 */
function _ems_basic_profile_final_menu() {

  install_menu_create_menu('Secondary links', '0');

  install_menu_update_menu_item(install_menu_get_mid('admin/settings'), array('title' => 'Settings'));
  install_menu_update_menu_item(install_menu_get_mid('admin/build'), array('title' => 'Build'));
  install_menu_update_menu_item(install_menu_get_mid('admin/user'), array('title' => 'User'));
  install_menu_update_menu_item(install_menu_get_mid('admin/content'), array('title' => 'Content'));

  // Send panels under build.
  install_menu_set_menu(install_menu_get_mid('admin/panels'), install_menu_get_mid('admin/build'));

  // Create "Media" menu
  $manage = install_menu_create_menu_item('admin/media', 'Media', install_menu_get_mid('admin'), 'Manage your websites media');
}

/*
 * Workflows, actions
 */
function _ems_basic_profile_final_workflow() {

  // Grab some role IDs we need.
  $administrator = install_get_rid(EMS_ROLE_ADMINISTRATOR);
  $maintainer = install_get_rid(EMS_ROLE_MAINTAINER);
  $owner = install_get_rid(EMS_ROLE_OWNER);

  $wid = install_workflow_create('Basic Processing', array($administrator, $maintainer, $owner,));
  // The first state '(creation)' will have been created with the ID 1.
  $created = 1;

  // Assign the new workflow to our content type 'contact'
  install_workflow_type_map_create('contact', $wid);

  // Create 2 new states
  $submitted = install_workflow_create_state($wid, 'Submitted');
  $processed = install_workflow_create_state($wid, 'Processed', 1);

  // Allow transitions by certain roles.
  $creation_transition = install_workflow_add_transition_role($created, $submitted, 'author');
  install_workflow_add_transition_role($submitted, $processed, $administrator);
  install_workflow_add_transition_role($submitted, $processed, $maintainer);
  install_workflow_add_transition_role($submitted, $processed, $owner);

  // Trigger actions housework that normally occurs the first time you visit the actions admin.
  $actions = actions_list();
  actions_synchronize($actions);  

  // Create a new redirect/message action.
  // This action is defined by the emspace_profile.module
  $params = array(
    'destination' => 'node',
    'message' => 'Thanks for your message, we\'ll get back to you soon.',
  );
  $redirect_action = install_action_create_action('action_emspace_submit', 'Contact form: Thanks', $params);

  // And finally assign this action to the appropriate transition.
  install_workflow_add_transition_action($creation_transition, $redirect_action);
}

/*
 * Views
 */
function _ems_basic_profile_final_views() {

  // TODO: export queue view from fsfm. Perhaps make a panels page...?

}

/*
 * Users, Roles
 */
function _ems_basic_profile_final_user() {

  /* Users, Roles, Permissions */

  // Creates a number of roles, we would probably delete most
  // roles depending on the client.
  install_add_role(EMS_ROLE_ADMINISTRATOR);
  install_add_role(EMS_ROLE_OWNER);
  install_add_role(EMS_ROLE_CONTRIBUTOR);
  install_add_role(EMS_ROLE_MAINTAINER);
  install_add_role(EMS_ROLE_EDITOR);

  $all_roles = user_roles();
  $rev_roles = array_flip($all_roles);

  install_add_user('admin', 'changeme', 'simon@emspace.com.au', array($rev_roles['Administrator'] => 'Administrator'), 1);

  // Assign permissions
  $all_permissions = module_invoke_all('perm');
  foreach ($all_roles AS $rid => $role) {
    switch ($role) {
      case 'anonymous user':
        install_set_permissions($rid, array('access content', 'access comments'));
        break;

      case EMS_ROLE_ADMINISTRATOR:
        install_set_permissions($rid, $all_permissions);
        break;

      case EMS_ROLE_MAINTAINER:
      case EMS_ROLE_OWNER:
      case EMS_ROLE_EDITOR:
      case EMS_ROLE_CONTRIBUTOR:
      case 'authenticated user':
      default:
        // For most users, we just want to assign safe permissions to make the job easier later.
        $filtered_perms = array();
        foreach ($all_permissions AS $key => $perm) {
          $perm = '_'. $perm; // so we never return 0 on a string match.
          // test for what we want...
          if (stripos($perm, 'access') || stripos($perm, 'edit own') || stripos($perm, 'post')) {
            // Looks good, now filter out dodgy permissions
            if (!(stripos($perm, 'php') || stripos($perm, 'admin') || stripos($perm, 'devel'))) {
              $filtered_perms[] = $all_permissions[$key];
            }
          }
        }
        install_set_permissions($rid, $filtered_perms);
        break;
    }
  }

}

